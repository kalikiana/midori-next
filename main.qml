import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtWebEngine 1.9

ApplicationWindow {
    id: window
    property ListModel tabs: ListModel {
        ListElement {
            url: "http://example.com"
            title: "Example"
        }
        ListElement {
            url: "https://www.midori-browser.org"
            title: "Midori Web Browser"
        }
        ListElement {
            url: "https://qt.io"
            title: "Qt Developer Website"
        }
    }

    visible: true
    minimumWidth: 100
    minimumHeight: 250
    width: Screen.width / 2
    height: Screen.height / 2
    property bool csd: true
    property bool showDecoration: csd && window.visibility == Window.Windowed
    flags: csd ? Qt.Window | Qt.CustomizeWindowHint : Qt.Window
    title: tabs.get(stack.currentIndex).title

    Component.onCompleted: {
        Qt.application.displayName = qsTr('Midori Web Browser')
    }

    header: ToolBar {

        // Unset background to use window background
        background: null
        RowLayout {
            anchors.fill: parent
            spacing: 0
            anchors.margins: 0

            TabBar {
                id: tabBar
                Layout.fillWidth: true
                // Drag space
                Layout.margins: showDecoration ? 8 : 0
                Layout.bottomMargin: 0

                Repeater {
                    model: tabs

                    TabButton {
                        text: title
                        Action {
                            shortcut: "Ctrl+W"
                            enabled: stack.currentIndex === model.index
                            onTriggered: tabs.remove(stack.currentIndex)
                        }
                    }
                }
            }

            ToolButton {
                icon.name: "tab-new-symbolic"
                icon.width: 16
                icon.height: 16
                background.implicitWidth: padding
                text: icon.height ? '' : "+"
                action: Action {
                    shortcut: "Ctrl+T"
                    onTriggered: tabs.append({url:"",title:"about:blank"})
                }
            }
            ToolButton {
                icon.name: "view-fullscreen-symbolic"
                icon.width: 16
                icon.height: 16
                background.implicitWidth: padding
                text: icon.height ? '' : "⇱︎"
                onClicked: window.visibility == Window.FullScreen ? window.showNormal() : window.showFullScreen()
            }
            ToolButton {
                icon.name: "open-menu-symbolic"
                icon.width: 16
                icon.height: 16
                background.implicitWidth: padding
                text: icon.height ? '' : "⋮"
                onClicked: menu.open()

                Menu {
                    id: menu
                    y: parent.height

                    Action { text: "&Clear private data"; shortcut: "Ctrl+Shift+Delete"; onTriggered: clearPrivateData.open(); }
                    Action { text: "Close a&l Windows"; shortcut: "Ctrl+Q"; onTriggered: Qt.quit(); }
                }
            }

            ToolButton {
                icon.name: "window-close-symbolic"
                icon.width: 16
                icon.height: 16
                text: icon.height ? '' : "x"
                // Drag space
                Layout.leftMargin: 8
                onClicked: window.close()
                visible: showDecoration
            }

            MouseArea {
                anchors.fill: parent
                z: -1
                property point prev
                onPressed: {
                    prev = Qt.point(mouse.x,mouse.y)
                }

                onDoubleClicked: window.visibility == Window.Maximized ? window.showNormal() : window.showMaximized()

                onPositionChanged: {
                    if(window.visibility == Window.Windowed && pressed) {
                        window.showNormal();
                        var delta = Qt.point(mouse.x - prev.x, mouse.y - prev.y);
                        window.x += delta.x;
                        window.y += delta.y;
                    }
                }
                enabled: csd
            }
        }
    }

    StackLayout {
        id: stack
        currentIndex: tabBar.currentIndex
        anchors.fill: parent

        Repeater {
            anchors.fill: parent
            model: tabs

            GridLayout {
                flow: GridLayout.TopToBottom
                ToolBar {

                    // Unset background to use window background
                    background: null
                    Layout.fillWidth: true
                    RowLayout {
                        anchors.fill: parent
                        ToolButton {
                            icon.name: "go-previous-symbolic"
                            icon.width: 16
                            icon.height: 16
                            text: icon.height ? '' : "<"
                            background.implicitWidth: padding
                            onClicked: webView.goBack()
                        }
                        ToolButton {
                            icon.name: "go-next-symbolic"
                            icon.width: 16
                            icon.height: 16
                            text: icon.height ? '' : ">"
                            background.implicitWidth: padding
                            onClicked: webView.goForward()
                        }
                        ToolButton {
                            icon.name: "view-refresh-symbolic"
                            icon.width: 16
                            icon.height: 16
                            text: icon.height ? '' : "@"
                            background.implicitWidth: padding
                            onClicked: webView.reload()
                            visible: !webView.loading
                        }
                        ToolButton {
                            icon.name: "process-stop-symbolic"
                            icon.width: 16
                            icon.height: 16
                            text: icon.height ? '' : "x"
                            background.implicitWidth: padding
                            onClicked: webView.stop()
                            visible: webView.loading
                        }

                        TextField {
                            id: urlBar
                            Layout.fillWidth: true
                            placeholderText: qsTr("Search or enter an address")
                            text: model.url
                            onAccepted: model.url = text
                        }
                    }
                }

                SplitView {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    orientation: Qt.Vertical

                    WebEngineView {
                        id: webView
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        SplitView.fillHeight: true
                        url: model.url
                        devToolsView: tools
                        onTitleChanged: model.title = title
                    }

                    WebEngineView {
                        id: tools
                        SplitView.preferredHeight: webView.inspectedView !== undefined ? 150 : 0
                    }

                }
                Action {
                    shortcut: "Ctrl+L"
                    enabled: stack.currentIndex === model.index
                    onTriggered: urlBar.forceActiveFocus()
                }
            }


        }
    }

    Drawer {
        id: panel
        y: header.height
        width: window.width * 0.3
        height: window.height - header.height
        position: 1.0
        ListView {
            anchors.fill: parent
            orientation: ListView.Vertical
            model: tabs
            delegate: TabButton {
                text: title
                onClicked: stack.currentIndex = index
            }

        }
    }

    Dialog {
        id: clearPrivateData
        title: qsTr("Clear Private Data")
        anchors.centerIn: parent
        modal: true

        Column {
            Label {
                text: qsTr("Clear the following data:")
            }

            Switch {
                id: clearWebCache
                text: qsTr("Web Cache")
            }
        }

        header: csd ? buttons : null
        footer: csd ? null : buttons
        property Item buttons: DialogButtonBox {
            Button {
                text: qsTr("&Clear private data")
                DialogButtonBox.buttonRole: DialogButtonBox.DestructiveRole
                onClicked: {
                    if (clearWebCache.checked)
                        WebEngine.defaultProfile.clearHttpCache()
                    clearPrivateData.close()
                }
            }
            Button {
                text: qsTr("&Cancel")
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
                onClicked: clearPrivateData.close()
            }
        }
    }
}
